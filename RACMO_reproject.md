# Reprojection of RACMO grid to geotiff projected files?

### Why?
Several regional climate data sets (including RACMO) use a rotated pole coordinate system. In such a rotated pole projection the climate grids are defined over the equator and then rotated to the area of interest. One of the advantages is that grid distance can be defined in fraction of degrees, which results in near equidistant grid points as long as the domain is small enough. The main disadvantage, however, is that the data are not easily imported in GIS programs or handled using standard geo-processing tools that cannot handle the rotated projection.

In this manual we handle to possible solutions. The first is based on GDAL and converts the rotated pole to Geotiffs. The [second method](#cdo-method) is based on CDO and reproject the netcdf to another projection system.


## GDAL solution
Luckily [gdal](https://gdal.org/) offers some tools to easily reproject the rotated pole data netcdf files to geotiff data in more standard projections. Information on the installation of gdal can be found on [https://gdal.org/download.html](https://gdal.org/download.html). Once you have gdal installed you can run a combination of two commands (`gdal_translate` and `gdalwarp`) to reproject the netcdf data.

### Requirements
 * [gdal](https://gdal.org/) with netcdf support. 
 * [ncdump](https://www.unidata.ucar.edu/software/netcdf/docs/netcdf_utilities_guide.html#ncdump_guide) [optional]

### Ncdump
Before convert the netcdf data variable to a geotiff file, we need to obtain some basic information from the netcdf file. Normally you should be able to retrieve this extent by opening the netcdf file in your favourite programming environment or by using [ncdump](https://www.unidata.ucar.edu/software/netcdf/docs/netcdf_utilities_guide.html#ncdump_guide). For example for our `t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H.nc`  file, we can see the data structure with `ncdump -h`:

```bash
ncdump -h t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H.nc

//#####################
// Output 
//#####################
netcdf t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H {
dimensions:
	rlon = 262 ;
	rlat = 240 ;
	height = 1 ;
	time = UNLIMITED ; // (29216 currently)
	bnds = 2 ;
	nblock1 = 40 ;
	nblock2 = 400 ;
variables:
	double lon(rlat, rlon) ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
	double lat(rlat, rlon) ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
	double dir(rlat, rlon) ;
		dir:long_name = "angle of rotation" ;
		dir:units = "degrees" ;
	double rlon(rlon) ;
		rlon:axis = "X" ;
		rlon:long_name = "longitude in rotated pole grid" ;
		rlon:standard_name = "grid_longitude" ;
		rlon:units = "degrees" ;
	double rlat(rlat) ;
		rlat:axis = "Y" ;
		rlat:long_name = "latitude in rotated pole grid" ;
		rlat:standard_name = "grid_latitude" ;
		rlat:units = "degrees" ;
	double height(height) ;
		height:axis = "Z" ;
		height:standard_name = "height" ;
		height:long_name = "height above the surface" ;
		height:units = "m" ;
		height:positive = "up" ;
	int block1(nblock1) ;
		block1:long_name = "GRIB Definition Block1" ;
	int block2(nblock2) ;
		block2:long_name = "GRIB Definition Block2" ;
	double time(time) ;
		time:axis = "T" ;
		time:units = "days since 1950-01-01 00:00:00.0" ;
		time:long_name = "time" ;
		time:dtgstart = "2001010100" ;
		time:bounds = "time_bnds" ;
		time:calendar = "standard" ;
	double time_bnds(time, bnds) ;
		time_bnds:units = "days since 1950-01-01 00:00:00.0" ;
	int dtg(time) ;
		dtg:long_name = "Verifying Datum-Time Group at Start of Interval" ;
		dtg:units = "yyyymmddhh" ;
	int date_bnds(time, bnds) ;
		date_bnds:long_name = "Verifying Date at Start and Finish of Interval" ;
		date_bnds:units = "yyyymmdd" ;
	int hms_bnds(time, bnds) ;
		hms_bnds:long_name = "Verifying Hour-Minute-Second at Start and Finish of Interval" ;
		hms_bnds:units = "hhmnss" ;
	int assigned(time) ;
		assigned:long_name = "Indicates whether data are stored (1) or not (0)" ;
		assigned:units = "1" ;
	float rotated_pole ;
		rotated_pole:grid_mapping_name = "rotated_latitude_longitude" ;
		rotated_pole:grid_north_pole_latitude = -180.f ;
		rotated_pole:grid_north_pole_longitude = -170.f ;
		rotated_pole:proj4_params = "-m 57.295779506 +proj=ob_tran +o_proj=latlon +o_lat_p=-180.0 +lon_0=10.0" ;
		rotated_pole:proj_parameters = "-m 57.295779506 +proj=ob_tran +o_proj=latlon +o_lat_p=-180.0 +lon_0=10.0" ;
		rotated_pole:projection_name = "rotated_latitude_longitude" ;
		rotated_pole:long_name = "projection details" ;
		rotated_pole:EPSG_code = "" ;
	float t2m(time, height, rlat, rlon) ;
		t2m:standard_name = "air_temperature" ;
		t2m:long_name = "2-m Temperature" ;
		t2m:units = "K" ;
		t2m:cell_methods = "time: instantaneous values" ;
		t2m:grid_mapping = "rotated_pole" ;
		t2m:coordinates = "lon lat" ;
		t2m:_FillValue = -9999.f ;

// global attributes:
		:Conventions = "CF-1.4" ;
		:source = "RACMO2" ;
		:Domain = "ANT27" ;
		:Experiment = "ERAINx_RACMO2.4.1" ;
		:institution = "Royal Netherlands Meteorological Institute (KNMI)" ;
		:CreationDate = "Fri Dec 30 00:17:44 2016" ;
		:comment = "asim2cdf: cpar=t2m, iwmo=11, ilvt=105, ilev=2, idh=3, ihacc=0, ihstep=3, itmode=-1, ivmode=0" ;
		:title = "2-m Temperature" ;
}
```
From this dump, we can obtain 3 important parameters, that we will need later:
 * `var`: the name of the data variable we want to convert. In this case we are interested in `t2m`
 * `rotated_pole:proj4_params`: the projection parameters of the rotated pole
 * `rlon` and `rlat`: the x/y-dimensions in the rotated pole

With `ncdump -v` we can now check the values of these x/y-dimensions:
```
ncdump -v rlon t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H.nc

//#####################
// Output 
//#####################
rlat = -30, -29.75, -29.5, -29.25, -29, -28.75, -28.5, -28.25, -28, -27.75, 
    -27.5, -27.25, -27, -26.75, -26.5, -26.25, -26, -25.75, -25.5, -25.25, 
    -25, -24.75, -24.5, -24.25, -24, -23.75, -23.5, -23.25, -23, -22.75, 
    -22.5, -22.25, -22, -21.75, -21.5, -21.25, -21, -20.75, -20.5, -20.25, 
    -20, -19.75, -19.5, -19.25, -19, -18.75, -18.5, -18.25, -18, -17.75, 
    -17.5, -17.25, -17, -16.75, -16.5, -16.25, -16, -15.75, -15.5, -15.25, 
    -15, -14.75, -14.5, -14.25, -14, -13.75, -13.5, -13.25, -13, -12.75, 
    -12.5, -12.25, -12, -11.75, -11.5, -11.25, -11, -10.75, -10.5, -10.25, 
    -10, -9.75, -9.5, -9.25, -9, -8.75, -8.5, -8.25, -8, -7.75, -7.5, -7.25, 
    -7, -6.75, -6.5, -6.25, -6, -5.75, -5.5, -5.25, -5, -4.75, -4.5, -4.25, 
    -4, -3.75, -3.5, -3.25, -3, -2.75, -2.5, -2.25, -2, -1.75, -1.5, -1.25, 
    -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 
    2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5, 4.75, 5, 5.25, 5.5, 5.75, 6, 
    6.25, 6.5, 6.75, 7, 7.25, 7.5, 7.75, 8, 8.25, 8.5, 8.75, 9, 9.25, 9.5, 
    9.75, 10, 10.25, 10.5, 10.75, 11, 11.25, 11.5, 11.75, 12, 12.25, 12.5, 
    12.75, 13, 13.25, 13.5, 13.75, 14, 14.25, 14.5, 14.75, 15, 15.25, 15.5, 
    15.75, 16, 16.25, 16.5, 16.75, 17, 17.25, 17.5, 17.75, 18, 18.25, 18.5, 
    18.75, 19, 19.25, 19.5, 19.75, 20, 20.25, 20.5, 20.75, 21, 21.25, 21.5, 
    21.75, 22, 22.25, 22.5, 22.75, 23, 23.25, 23.5, 23.75, 24, 24.25, 24.5, 
    24.75, 25, 25.25, 25.5, 25.75, 26, 26.25, 26.5, 26.75, 27, 27.25, 27.5, 
    27.75, 28, 28.25, 28.5, 28.75, 29, 29.25, 29.5, 29.75 ;
```
```
ncdump -v rlat t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H.nc

//#####################
// Output 
//#####################
rlon = -32.75, -32.5, -32.25, -32, -31.75, -31.5, -31.25, -31, -30.75, 
    -30.5, -30.25, -30, -29.75, -29.5, -29.25, -29, -28.75, -28.5, -28.25, 
    -28, -27.75, -27.5, -27.25, -27, -26.75, -26.5, -26.25, -26, -25.75, 
    -25.5, -25.25, -25, -24.75, -24.5, -24.25, -24, -23.75, -23.5, -23.25, 
    -23, -22.75, -22.5, -22.25, -22, -21.75, -21.5, -21.25, -21, -20.75, 
    -20.5, -20.25, -20, -19.75, -19.5, -19.25, -19, -18.75, -18.5, -18.25, 
    -18, -17.75, -17.5, -17.25, -17, -16.75, -16.5, -16.25, -16, -15.75, 
    -15.5, -15.25, -15, -14.75, -14.5, -14.25, -14, -13.75, -13.5, -13.25, 
    -13, -12.75, -12.5, -12.25, -12, -11.75, -11.5, -11.25, -11, -10.75, 
    -10.5, -10.25, -10, -9.75, -9.5, -9.25, -9, -8.75, -8.5, -8.25, -8, 
    -7.75, -7.5, -7.25, -7, -6.75, -6.5, -6.25, -6, -5.75, -5.5, -5.25, -5, 
    -4.75, -4.5, -4.25, -4, -3.75, -3.5, -3.25, -3, -2.75, -2.5, -2.25, -2, 
    -1.75, -1.5, -1.25, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 
    1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5, 4.75, 5, 
    5.25, 5.5, 5.75, 6, 6.25, 6.5, 6.75, 7, 7.25, 7.5, 7.75, 8, 8.25, 8.5, 
    8.75, 9, 9.25, 9.5, 9.75, 10, 10.25, 10.5, 10.75, 11, 11.25, 11.5, 11.75, 
    12, 12.25, 12.5, 12.75, 13, 13.25, 13.5, 13.75, 14, 14.25, 14.5, 14.75, 
    15, 15.25, 15.5, 15.75, 16, 16.25, 16.5, 16.75, 17, 17.25, 17.5, 17.75, 
    18, 18.25, 18.5, 18.75, 19, 19.25, 19.5, 19.75, 20, 20.25, 20.5, 20.75, 
    21, 21.25, 21.5, 21.75, 22, 22.25, 22.5, 22.75, 23, 23.25, 23.5, 23.75, 
    24, 24.25, 24.5, 24.75, 25, 25.25, 25.5, 25.75, 26, 26.25, 26.5, 26.75, 
    27, 27.25, 27.5, 27.75, 28, 28.25, 28.5, 28.75, 29, 29.25, 29.5, 29.75, 
    30, 30.25, 30.5, 30.75, 31, 31.25, 31.5, 31.75, 32, 32.25, 32.5 ;

```
From this we can conclude that rlon ranges from -32.75 to 32.5 and rlat from -30 to 29.75.

### Gdal_translate
Once we know this `var` name and `extent`, we can run [gdal_translate](https://gdal.org/programs/gdal_translate.html) to convert the netcdf file to a temporary unprojected geotiff:
```bash
gdal_translate NETCDF:”filename.nc”:var -a_ullr x_ul_in_rotatedpole y_ul_in_rotatedpole x_lr_in_rotatedpole y_lr_in_rotatedpole  var_temp.tif
```
In our example, we want to reproject the `t2m` variable:


```bash
gdal_translate NETCDF:"t2m.KNMI-2001.ANT27.ERAINx_RACMO2.4.1.3H.nc":t2m -a_ullr -32.75 29.75 32.5 -30.0  var_temp.tif
```

### Gdalwarp
After converting the netcdf file to a temporary unprojected geotiff, we can use [gdalwarp](https://gdal.org/programs/gdalwarp.html) to assign the projection we are interested in. This implies that you know:
 * `rotated_pole:proj4_params`: the `rotated_pole:proj4_params` from the input file, although they might need some modification. In one of example, we needed to force a 90degree rotation in order to fit to Antarctic Stereographic and therefore we need to add 90degrees to `+lon_0` 
 * `t_srs`: the output coordinate systems, which can either be a EPSG-code, proj4 parameters, etc
 * `ulx` `uly` `lrx` `lry`: the upper left (ul), lower right (lr) x/y values
 * `xres` `yres`: the desired output resolution 
 * 'interpolation_method: the resampling method to use. Available methods are: near (default), bilinear, cubic, cubicspline, lanczos, average, mode,  max, min, med, Q1, Q3 (Check [gdalwarp](https://gdal.org/programs/gdalwarp.html) for more info).

 In psuedo code, our gdalwarp code will look like
```bash
gdalwarp -s_srs "rotated_pole:proj4_params" -t_srs “t_srs" -te xmin ymin xmax ymax -tr xres yres -r interpolation_method var_temp.tif outfile.tif
```
In our t2m example, where we want to reproject op Antarctic Stereographic (EPSG:3031) and a 27km grid it will be:
```bash
gdalwarp -s_srs "-m 57.295779506 +proj=ob_tran +o_proj=latlon +o_lat_p=-180.0 +lon_0=10.0" -t_srs "EPSG:3031" -te -3051000 3051000 3051000 -3051000  -tr 27000 -27000 -r near var_temp.tif alb.tif
```

### Done
This is it. Your output file should be nicely projected (and you can remove the temporary `var_temp.tif` file) and ready to be imported in your favourite program.

## CDO method
[CDO](https://code.mpimet.mpg.de/projects/cdo) also offers tools to easily reproject the rotated pole data netcdf files in more standard projections. Information on the installation of CDO can be found on [https://code.mpimet.mpg.de/projects/cdo/files](https://code.mpimet.mpg.de/projects/cdo/files). Once you have gdal installed you can run a combination of two tools to reproject the netcdf data.

### Requirements
 * [Google Earth Engine script](https://code.earthengine.google.com/4ca4732e2fa387b23bdef6ce3160d526) to create an output file in the required projection system and that contains for every pixel the desired latlon coordinates. Within this script you need to set 3 variables (i.e. bounding box of output, output coordinate system, and resolution). The script will generate an output file `LatLon_coordinates.tif` covering your desired output box and resolution with for every pixel the corresponding latlon variables as two layers. 
  * [NCL](https://www.ncl.ucar.edu/)
  * [CDO](https://code.mpimet.mpg.de/projects/cdo)

### GEE export
Before convert the netcdf data we need a file in the desired projection/resolution that contains for every pixel the corresponding latlon values. This file can be easily created using this [Google Earth Engine script](https://code.earthengine.google.com/4ca4732e2fa387b23bdef6ce3160d526).

### CDO conversion
This `LatLon_coordinates.tif` needs to be converted first to netcdf in order to let CDO read the pixel latlon coordinates. This conversion can be done using `gdal_translate`:

```bash
gdal_translate LatLon_coordinates.tif -of netCDF LatLon_coordinates.nc
```

Subsequently we can use `ncl` to create resampling files that creates a netcdf file that contains both the latlon grids for the input data (e.g. RACMO data) and output data (i.e. the `LatLon_coordinates.nc` file we just created). Therefore we need to write a script (e.g. `NCL_resampling.txt`) like this in your favourite text editor and add it to the folder where you have stored both the `LatLon_coordinates.nc` and your rotated pole file (`RACMO_file.nc` in this example).

```bash
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

begin
   targetGridName = "EPSG_SCRIP.nc"
   latlon = "LatLon_coordinates.nc" ; File that contains 2D Lat and Lon fields in target projection (e.g. for known EPSG)
   f1 = addfile(latlon, "r")
   Opt                = True
   Opt@ForceOverwrite = True
   Opt@PrintTimings   = True
   Opt@Title          = "Output grid in Lon/Lat coordinates"
   Opt@NetCDFType    = "netcdf4"
   curvilinear_to_SCRIP(targetGridName,f1->Band2,f1->Band1,Opt)

   sourceGridName = "RACMO_SCRIP.nc"
   latlon = "RACMO_ANT27_t2m_season.nc" ; File in rotated pole coordinates that contains projection with corresponding Lat and Lon fields
   f2 = addfile(latlon, "r")
   Opt                = True
   Opt@ForceOverwrite = True
   Opt@PrintTimings   = True
   Opt@Title          = "RACMO grid in Lon/Lat coordinates"
   Opt@NetCDFType    = "netcdf5"
   curvilinear_to_SCRIP(sourceGridName,f2->lat,f2->lon,Opt)
end
```

Subsequently we have execute this ncl script with:

```bash
ncl NCL_resampling.txt
```
 The result of this script is two netcdf files `EPSG_SCRIP.nc` and `RACMO_SCRIP.nc` that can be put into the CDO resampling. This can be done with the remap command, where we can choose between bilinear interpolation (`remapbil`), bicubic interpolation (`remapbic`), distance-weighted averaging (`remapdis`). For example:

 ```bash
 cdo remapbil,EPSG_SCRIP.nc -setgrid,RACMO_SCRIP.nc RACMO_file.nc outputfile.nc
 ```

 More information can be found on [http://www.climate-cryosphere.org/wiki/index.php?title=Regridding_with_CDO](http://www.climate-cryosphere.org/wiki/index.php?title=Regridding_with_CDO).

 ### Done
This is it. Your netcdf file should be nicely projected (and you can remove the temporary filess) and ready to be imported in your favourite program.

